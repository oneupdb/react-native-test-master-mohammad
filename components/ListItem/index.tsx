import React from 'react';
import {
  View, Text, Image, StyleSheet,
} from 'react-native';

interface Props {
  firstName: string;
  avatar: string;
}

const AVATAR_SIZE = 64;

const ListItem: React.FC<Props> = ({ firstName, avatar }) => (
  <View style={styles.container}>
    <View style={styles.avatarWrapper}>
      <Image style={styles.avatar} source={{ uri: avatar }} />
    </View>
    <View style={styles.infoWrapper}>
      <Text style={styles.name}>{firstName}</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    height: 81,
    width: '100%',
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#eeeeee',
  },
  avatarWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE / 2,
  },
  infoWrapper: {
    flex: 4,
    justifyContent: 'center',
  },
  name: {
    fontSize: 16,
    paddingLeft: 24,
  },
});

export default ListItem;
