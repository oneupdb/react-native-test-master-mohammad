import React from 'react';
import { View, StyleSheet } from 'react-native';
import Animated, { Easing } from 'react-native-reanimated';

const {
  Clock,
  Value,
  set,
  cond,
  startClock,
  clockRunning,
  timing,
  block,
  eq,
  not,
  interpolate,
  useCode,
} = Animated;

const runTiming = (clock: Animated.Clock, duration: number, toValue: number) => {
  const state = {
    finished: new Value(0),
    position: new Value(0),
    time: new Value(0),
    frameTime: new Value(0),
  };

  const config = {
    duration,
    toValue,
    easing: Easing.inOut(Easing.ease),
  };

  return block([
    cond(clockRunning(clock), 0, [
      // If the clock isn't running we reset all the animation params and start the clock
      set(state.finished, 0),
      set(state.time, 0),
      set(state.position, 0),
      set(state.frameTime, 0),
      startClock(clock),
    ]),
    // we run the step here that is going to update position
    timing(clock, state, config),
    // if the animation is over we stop the clock
    // cond(state.finished, debug('stop clock', stopClock(clock))),

    // if animate is over, reset the values to repeat
    cond(eq(state.finished, 1), [
      set(state.finished, 0),
      set(state.time, 0),
      set(state.position, 0),
      set(state.frameTime, 0),
    ]),
    // we made the block return the updated position
    state.position,
  ]);
};

const PulseLoading: React.FC = () => {
  const clock = new Clock();
  const scaleProgress = new Value(0);
  const opacityProgress = new Value(1);
  const scale2Progress = new Value(0);
  const opacity2Progress = new Value(1);

  useCode(() => block([
    cond(not(clockRunning(clock)), startClock(clock)),
    set(scaleProgress, runTiming(clock, 3000, 2)),
    set(opacityProgress, runTiming(clock, 3000, 1)),
    set(scale2Progress, runTiming(clock, 2000, 2)),
    set(opacity2Progress, runTiming(clock, 2000, 1)),
  ]), [scaleProgress, opacityProgress, scale2Progress, opacity2Progress, clock]);

  const scale = interpolate(scaleProgress, {
    inputRange: [0, 1],
    outputRange: [0, 1],
  });
  const opacity = interpolate(opacityProgress, {
    inputRange: [0, 1],
    outputRange: [0.5, 0],
  });
  const scale2 = interpolate(scale2Progress, {
    inputRange: [0, 1],
    outputRange: [0, 1],
  });
  const opacity2 = interpolate(opacity2Progress, {
    inputRange: [0, 1],
    outputRange: [0.5, 0],
  });

  return (
    <View style={styles.container}>
      <Animated.View
        style={styles.staticCircle}
      />
      <Animated.View
        style={[styles.circle, { opacity, transform: [{ scale }] }]}
      />
      <Animated.View
        style={[styles.circle, { opacity: opacity2, transform: [{ scale: scale2 }] }]}
      />
    </View>
  );
};

const CIRCLE_SIZE = 100;
const STATIC_CIRCLE_SIZE = 30;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFill as StyleSheet,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  circle: {
    backgroundColor: 'green',
    height: CIRCLE_SIZE,
    width: CIRCLE_SIZE,
    borderRadius: CIRCLE_SIZE,
    position: 'absolute',
  },
  staticCircle: {
    backgroundColor: 'green',
    height: STATIC_CIRCLE_SIZE,
    width: STATIC_CIRCLE_SIZE,
    borderRadius: STATIC_CIRCLE_SIZE / 2,
    position: 'absolute',
  },
});

export default PulseLoading;
