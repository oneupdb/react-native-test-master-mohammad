import React, { useEffect } from 'react';
// import { Dispatch } from 'redux';
import {
  FlatList, StyleSheet, Text, Platform, ToastAndroid, Alert,
} from 'react-native';
import { connect, useDispatch } from 'react-redux';
import { ApplicationState } from '../store';
import { User } from '../store/users/types';
import { getUsersAction, setUsersNextPageAction } from '../store/users/actions';
import { usePrevious } from '../hooks';
import PulseLoading from '../components/PulseLoading';
import ListItem from '../components/ListItem';

interface ListItemTypes {
    item: User;
}

const notifyMessage = (msg: string): void => {
  if (Platform.OS === 'android') {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  } else {
    Alert.alert(msg);
  }
};

const UsersListScreen: React.FC<ApplicationState> = (props) => {
  const {
    users, loading, page, message,
  } = props.usersState;

  const prevNotificationV = usePrevious(message?.v);

  const dispatch = useDispatch();

  const handleOnEndReached = () => dispatch(setUsersNextPageAction());
  useEffect(() => {
    dispatch(getUsersAction(1));
  }, []);
  useEffect(() => {
    // notify if there are no users
    if (prevNotificationV !== message?.v) {
      notifyMessage(message.notify);
    }
  }, [page, message?.v]);
  const renderUsersAndLoading = () => {
    if (loading) {
      return <PulseLoading />;
    } else if (users.length) {
      return (
        <FlatList<any>
          style={{ ...StyleSheet.absoluteFill as StyleSheet }}
          showsVerticalScrollIndicator={false}
          data={users}
          keyExtractor={(item) => item.id.toString()}
          extraData={users}
          onEndReached={handleOnEndReached}
          onEndReachedThreshold={0.05}
          renderItem={({ item }: ListItemTypes) => (
            <ListItem
              firstName={item.first_name}
              avatar={item.avatar}
            />

          )}
        />
      );
    } else {
      return <Text>No Users</Text>;
    }
  };
  return renderUsersAndLoading();
};

const mapStateToProps = (state: ApplicationState) => ({ usersState: state.usersState });
export default connect(mapStateToProps)(UsersListScreen);
