import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { UsersState } from './users/types';
import usersReducer from './users/reducer';

// merge all states types
export interface ApplicationState {
    usersState: UsersState;
}

const rootReducer = combineReducers({
  usersState: usersReducer,
});

export const store = configureStore({ reducer: rootReducer });
