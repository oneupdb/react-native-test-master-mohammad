export interface User {
    id: number | string;
    first_name: string;
    avatar: string;
    [propName: string]: any;
}
export interface Users {
    users: Array<any>;
}
export interface UsersState extends Users {
    page?: number;
    loading?: boolean;
    message?: {
        notify: string;
        v: number;
    }
}
export interface GetUsersAction {
    type: string;
    payload: UsersState
}
export interface SetUsersNextPageAction {
    type: string;
    payload: {
        page: number
    };
}
export interface UsersRequestIsLoadingAction {
    type: string;
    payload: {
        loading: boolean;
    };
}
export interface SetUsersAction {
    type: string;
    payload: {
        users: Users;
    }
}
export interface SetGetUsersApiNotificationV {
    type: string;
    payload: {
        v: number;
    }
}

export type UsersActions =
GetUsersAction |
SetUsersNextPageAction |
UsersRequestIsLoadingAction |
SetUsersAction |
SetGetUsersApiNotificationV;
