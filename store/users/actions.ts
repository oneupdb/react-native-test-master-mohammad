import { constants as c } from './constants';
import { Users, SetUsersAction, UsersRequestIsLoadingAction } from './types';
import { getUsersApi } from '../../api/users';
import { ApplicationState } from '../index';

const setUsersAction = (users: Users): SetUsersAction => ({
  type: c.SET_USERS,
  payload: {
    users,
  },
});

export const getUsersAction = (page: number) => (dispatch: any, getState: ApplicationState) => {
  const stateBefore = getState().usersState;

  dispatch(usersRequestIsLoadingAction(true));
  getUsersApi(page, async (res: any) => {
    if (res.data) {
      if (res.data.length) {
        await dispatch(setUsersAction(stateBefore.users.concat(res.data)));
      } else {
        await dispatch(setUsersAction(res.data));
      }

      // loading for three seconds more
      setTimeout(async () => {
        await dispatch(usersRequestIsLoadingAction(false));
      }, 3000);
    } else {
      // loading for three seconds more
      setTimeout(async () => {
        await dispatch(usersRequestIsLoadingAction(false));
      }, 3000);
    }
  });
};

export const usersRequestIsLoadingAction = (loading: boolean): UsersRequestIsLoadingAction => ({
  type: c.USERS_REQUEST_IS_LOADING,
  payload: { loading },
});
export const setUsersNextPageAction = () => async (dispatch: any, getState: ApplicationState) => {
  const { page, users } = getState().usersState;
  const prevPage = page;
  const nextPage = prevPage + 1;

  await dispatch({
    type: c.SET_USERS_NEXT_PAGE,
    payload: {
      page: nextPage,
    },
  });

  if (prevPage !== nextPage) {
    getUsersApi(nextPage, (res: any): void => {
      if (res.data.length) {
        dispatch(setUsersAction(users.concat(res.data)));
      } else {
        dispatch({
          type: c.SET_GET_USERS_API_NOTIFICATION_V,
          payload: {
            v: nextPage,
          },
        });
      }
    });
  }
};
