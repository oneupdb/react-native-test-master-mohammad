import { constants as c } from './constants';
import { UsersState, UsersActions } from './types';

const initialState = {
  users: [],
  page: 1,
  loading: false,
  message: {
    notify: 'No more users!',
    v: 1,
  },
};

export default (state: UsersState = initialState, action: any) => {
  switch ((action as UsersActions).type) {
    case c.SET_USERS:
      return {
        ...state,
        users: action.payload.users,
      };
    case c.USERS_REQUEST_IS_LOADING:
      return {
        ...state,
        loading: action.payload.loading,
      };
    case c.SET_USERS_NEXT_PAGE:
      return {
        ...state,
        page: action.payload.page,
      };
    case c.SET_GET_USERS_API_NOTIFICATION_V:
      return {
        ...state,
        message: {
          ...state.message,
          v: action.payload.v,
        },
      };
    default:
      return state;
  }
};
