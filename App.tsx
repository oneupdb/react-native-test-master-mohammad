/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { Provider } from 'react-redux';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
} from 'react-native';
import { store } from './store';
import UsersListScreen from './screens/UsersListScreen';

const App = () => (
  <>
    <StatusBar barStyle="dark-content" />
    <SafeAreaView style={styles.mainContainer}>
      <Provider store={store}>
        <UsersListScreen />
      </Provider>
    </SafeAreaView>
  </>
);

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;
