export const GET_USERS = (page: number): string => `https://reqres.in/api/users?page=${page}&per_page=10`;
