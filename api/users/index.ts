import * as url from './urls';

export const getUsersApi = (page: number, cb: Function): any => {
  fetch(url.GET_USERS(page))
    .then((res) => res.json())
    .then((json) => cb(json))
    .catch((error) => cb(error));
};
